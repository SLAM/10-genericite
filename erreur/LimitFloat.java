public class LimitFloat {

    //Variables d'instance
    private Float valeur;
    private Float min;
    private Float max;

    //Constructeur par défaut
    public LimitFloat() {
        this.valeur = null;
        this.min = null;
        this.max = null;
    }

    //Constructeur précisant le min et max
    public LimitFloat(Float min, Float max) {
        this.valeur = null;
        this.min = min;
        this.max = max;
    }

    //Définit la valeur avec le paramètre
    public void setValeur(Float val) {
        if ((this.min != null) && (this.max != null)) {
            if ((val > this.min) && (val < this.max)) {
                this.valeur = val;
            }
        }
    }

    //Retourne la valeur
    public Float getValeur() {
        return this.valeur;
    }
}
