public class App {
    public static void main(String[] args) {
        LimitInt val1 = new LimitInt(1, 12);
        val1.setValeur(5);
        System.out.println("Valeur entière: " + val1.getValeur());
        LimitFloat val2 = new LimitFloat(3.14f, 3.18f);
        val2.setValeur(3.15f);
        System.out.println("Valeur réelle: " + val2.getValeur());
    }
}
