public class LimitInt {

    //Variables d'instance
    private Integer valeur;
    private Integer min;
    private Integer max;

    //Constructeur par défaut
    public LimitInt() {
        this.valeur = null;
        this.min = null;
        this.max = null;
    }

    //Constructeur précisant le min et max
    public LimitInt(Integer min, Integer max) {
        this.valeur = null;
        this.min = min;
        this.max = max;
    }

    //Définit la valeur avec le paramètre
    public void setValeur(Integer val) {
        if ((this.min != null) && (this.max != null)) {
            if ((val > this.min) && (val < this.max)) {
                this.valeur = val;
            }
        }
    }

    //Retourne la valeur
    public Integer getValeur() {
        return this.valeur;
    }
}
