public class LimitNumber<T extends Number & Comparable<? super T>> {
    //L'interface Comparable est valable pour les types T ou parents de T

    //Variables d'instance
    private T valeur;
    private T min;
    private T max;

    //Constructeur par défaut
    public LimitNumber() {
        this.valeur = null;
        this.min = null;
        this.max = null;
    }

    //Constructeur précisant le min et max
    public LimitNumber(T min, T max) {
        this.valeur = null;
        this.min = min;
        this.max = max;
    }

    //Définit la valeur avec le paramètre
    public void setValeur(T val) {
        if ((this.min != null) && (this.max != null)) {
            if ((val.compareTo(this.min) > 0) && (val.compareTo(this.max) < 0)) {
                this.valeur = val;
            }
        }
    }

    //Retourne la valeur déjà « castée » par la signature de la méthode !
    public T getValeur() {
        return this.valeur;
    }
}
