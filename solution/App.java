public class App {
    public static void main(String[] args) {
        LimitNumber<Integer> val1 = new LimitNumber<Integer>(1, 12);
        val1.setValeur(5);
        System.out.println("Valeur entière: " + val1.getValeur());
        LimitNumber<Float> val2 = new LimitNumber<Float>(3.14f, 3.18f);
        val2.setValeur(3.15f);
        System.out.println("Valeur réelle: " + val2.getValeur());
    }
}
