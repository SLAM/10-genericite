# Appréhendez la généricité en Java

Nous abordons ce concept en énonçant le besoin suivant : créer un type
permettant de limiter les valeurs possibles pour un nombre (l'ensemble
des valeurs possibles est défini par un minimal et maximal).

## Solution classique
Dans le sous-répertoire `classique`, vous trouverez une solution basique,
qui limite les valeurs possibles à des entiers.

## Implémentation erronée
Une erreur possible, que vous trouverez implémenter dans le sous-répertoire `erreur`,
serait alors d'implémenter autant de classes que de types possibles.

## Solution adéquat
Nous pouvons aussi définir une classe ayant un type générique en paramètre.
Le type fourni devra néanmoins implémenter les interfaces `Number` et `Comparable`.
L'interface `Comparable`  sera valable pour les types `T` ou parents de `T`.

Vous trouverez cette solution dans le sous-répertoire bien nommé `solution`
